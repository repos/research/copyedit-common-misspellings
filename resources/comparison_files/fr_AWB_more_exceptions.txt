<Typo word="ref,ref" find="(</ref>|<ref[^/]*/>) *(<sup>)?[,;\.]?(</sup>)? *<ref\b" replace="$1{{,}}<ref" />
<Typo word="espace avant ref" find="([^|]) <ref" replace="$1<ref" />
<Typo word="Siècle av. J.-C." find="\b([12]?\d) *(?:<sup(?: +class=\"exposant\")?>)?i?[éeè°](?:me)?(?:</sup>)? *[sS](?:i[eèé]cle|\.) +av(?:ant|\.) +J(?:ésus|\.)?[- ]?C(?:hrist\b|\.|\b)" replace="{{-s-|{{subst:Nombre en romain|$1}}}}" />
<Typo word="Siècle av. J.-C." find="\b([XVI]+) *(?:<sup(?: +class=\"exposant\")?>)?i?[éeè°](?:me)?(?:</sup>)? *[sS](?:i[eèé]cle|\.) +av(?:ant|\.) +J(?:ésus|\.)?[- ]?C(?:hrist\b|\.|\b)" replace="{{-s-|$1}}" />
<Typo word="Siècle" find="\b([12]?\d) *(?:<sup(?: +class=\"exposant\")?>)?i?[éeè°](?:me)?(?:</sup>)? *[sS](?:i[eèé]cle\b|\.)" replace="{{s-|{{subst:Nombre en romain|$1}}}}" />
<Typo word="Siècle" find="\b([XVI]+) *(?:<sup(?: +class=\"exposant\")?>)?i?[éeè°](?:me)?(?:</sup>)? *[sS](?:i[eèé]cle\b|\.)" replace="{{s-|$1}}" />
<Typo word="ap. J.-C." find="\bap\. +J\.[- ]?C\." replace="ap. J.-C." />
<Typo word="vitesse" find="(k?m)\.([sh])-([1-4])" replace="$1.$2<sup>-$3</sup>" />
<Typo word="vitesse" find="(k?m)\/([sh])([1-4])" replace="{{fchim|H|2|O}}" /> 
<Typo word="vitesse" find="(k?m)\/([sh])<sup>([1-4])</sup>" replace="$1.$2<sup>-$3</sup>" />
<Typo word="Unité mètre + exposant" find="([0-9]+)(&nbsp;| +)?([0-9]+)?[.,]([0-9]+)(&nbsp;| +)?(cm|km|m|mm)(<sup>|{{|{{exp\|)?([2-9])(</sup>|}})?" replace="{{unité|$1$3.$4|$6|$8}}" />
<Typo word="Unité mètre + exposant" find="([0-9]+)(&nbsp;| +)?([0-9]+)?(&nbsp;| +)?([0-9]+)?(&nbsp;| +)?(cm|km|m|mm)(<sup>|{{|{{exp\|)?([2-9])(</sup>|}})?" replace="{{unité|$1$3$5|$7|$9}}" />
<Typo word="Unité mètre + exposant" find="([0-9]+)(&nbsp;| +)?([0-9]+)?[.,]([0-9]+)(&nbsp;| +)?(cm|km|m|mm|Mm)(<sup>|{{|{{exp\|)?²(</sup>|}})?" replace="{{unité|$1$3.$4|$6|2}}" />
<Typo word="Unité mètre + exposant" find="([0-9]+)(&nbsp;| +)?([0-9]+)?(&nbsp;| +)?([0-9]+)?(&nbsp;| +)?(cm|km|m|mm|Mm)(<sup>|{{|{{exp\|)?²(</sup>|}})?" replace="{{unité|$1$3$5|$7|2}}" />
<Typo word="Unité mètre + exposant" find="([0-9]+)(&nbsp;| +)?([0-9]+)?[.,]([0-9]+)(&nbsp;| +)?(cm|km|m|mm|Mm)(<sup>|{{|{{exp\|)?³(</sup>|}})?" replace="{{unité|$1$3.$4|$6|3}}" />
<Typo word="Unité mètre + exposant" find="([0-9]+)(&nbsp;| +)?([0-9]+)?(&nbsp;| +)?([0-9]+)?(&nbsp;| +)?(cm|km|m|mm|Mm)(<sup>|{{|{{exp\|)?³(</sup>|}})?" replace="{{unité|$1$3$5|$7|3}}" />
<Typo word="<br />" find="</[bB][rR] ?>" replace="<br />" />
<Typo word=", " find="(?<=[^0-9 {]) *,(?![\s &])(?!<br)" replace=", " />
<Typo word="Coord" find="(-?) *([0-9]{1,3}) *° *(-?) *([0-9]{1,2}) *[\'\′] *(-?) *([0-9]{1,2}) *[\"\″] *([NS]) *,? *(-?) *([0-9]{1,3}) *° *(-?) *([0-9]{1,2}) *[\'\′] *(-?) *([0-9]{1,2}) *[\"\″] *([EW])" replace="{{Coord|$1$2|$3$4|$5$6|$7|$8$9|$10$11|$12$13|$14}}" />