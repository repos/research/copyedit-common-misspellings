# copyedit-common-misspellings

This repo contains code to 
* Automatically curate lists of common misspellings in different languages from parsing Wiktionary.
* Use these lists to identify copyedits in Wikipedia articles.

You can find more details about the project and the results in [Research:Copyediting_as_a_structured_task/Common_misspellings_wiktionary](https://meta.wikimedia.org/wiki/Research:Copyediting_as_a_structured_task/Common_misspellings_wiktionary)

## Repository Structure
```
.
├── notebooks: Contains notebooks for parsing, extracting, and analyzing misspellings.
├── scripts: Contains scripts that parse and extract misspellings. These are ultimately used in notebooks.
├── resources: Contains misspellings extracted from Wiktionaries.
│   ├── comparison_files: Intermediate analysis to compare extracted misspellings with existing misspelling lists.
│   └── *other files: misspellings extracted from Wiktionaries*
├── outputs: Contains outputs from extraction and analysis of misspellings.
│   ├── analysis_outputs: Files from Wikipedia misspelling analysis.
│   ├── figures: Contains analysis figures.
│   └── *other files: misspellings detected in Wikipedias*
├── requirements.txt
└── README.md
```

## Code
The final notebooks that handle all language Wiktionary and Wikipedia at once are shown in bold in the list below. The scripts and notebooks in this repository use [WMF internal cluster](https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Clients). With some modifications in the code, one can use public dumps from Wikipedia and Wiktionary as well.


### Parsing misspellings from Wiktionary
- [parse_wiktionary_misspellings_en.ipynb](notebooks/parse_wiktionary_misspellings_en.ipynb): Extracts and analyzes misspellings from enwiktionary. Uses [extract_wiktionary_misspellings.py](scripts/extract_wiktionary_misspellings.py).
- **[parse_wiktionary_misspellings_all_wiki.ipynb](notebooks/parse_wiktionary_misspellings_all_wiki.ipynb)**: Extracts misspellings from Wiktionaries. Uses [extract_wiktionary_misspellings.py](scripts/extract_wiktionary_misspellings.py).

### Comparing parsed misspellings with existing sources
- [compare_misspellings_with_existing_lists_en_fr.ipynb](notebooks/compare_misspellings_with_existing_lists_en_fr.ipynb): Compares misspellings extracted from enwiktionary with existing lists. Uses and saves files in `resources/comparison_files/`.

### Detecting misspellings in Wikipedia
- [parse_wikipedia_extract_misspellings_simplewiki.ipynb](notebooks/parse_wikipedia_extract_misspellings_simplewiki.ipynb): Detects and analyzes misspellings in Simple Wikipedia. Uses [extract_wikipedia_misspellings.py](scripts/extract_wikipedia_misspellings.py).
- [analyze_misspellings_on_test_wikipedias_en_de_fr.ipynb](notebooks/analyze_misspellings_on_test_wikipedias_en_de_fr.ipynb): Analyzes misspellings in en, de, and fr Wikipeda. Detection not shown, Wikipedia language can be changed [here](notebooks/parse_wikipedia_extract_misspellings_simplewiki.ipynb) to easily extract misspelling from any language.
- **[parse_wikipedia_extract_misspellings_all_wiki.ipynb](notebooks/parse_wikipedia_extract_misspellings_all_wiki.ipynb)**: Detects misspellings in all Wikipedia. Uses [extract_wikipedia_misspellings.py](scripts/extract_wikipedia_misspellings.py).
- **[analyze_misspellings_all_wiki.ipynb](notebooks/analyze_misspellings_all_wiki.ipynb)**: Analyzes misspellings detected in all Wikipedias.

## Other work

Some initial steps towards a different direction includes using [Template:R_from_misspelling](https://en.wikipedia.org/wiki/Template:R_from_misspelling) in Wikipedia. Another step could be to check the redirects in Wiktionary itself.

For [Template:R_from_misspelling](https://en.wikipedia.org/wiki/Template:R_from_misspelling), we do some initial analysis in [misspelling_redirects.ipynb](notebooks/misspelling_redirects.ipynb) and list the misspellings in [WP_misspelling_redirects.tsv](outputs/redirects_analysis/WP_misspelling_redirects.tsv). We find that the list contains typical single word misspellings, but also contains word-replacements, misspelling correction in one word in a phrase, phrase changes, capitalization or punctuation changes, etc. There are **~50k** misspellings detected total, of which 10k are single word changes/corrections.

Checking redirects in Wiktionary we find that Wiktionary has a rule to NOT use redirects for misspellings. See [Wiktionary:Redirections](https://en.wiktionary.org/wiki/Wiktionary:Redirections). So the redirects we found in Wiktionary mostly contained phrase, space, or special character changes. No misspellings were found.