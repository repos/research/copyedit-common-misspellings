import wmfdata
import mwparserfromhell
from functools import reduce
from pyspark.sql.types import *
import pyspark.sql.functions as F
from pyspark.sql import DataFrame

spark = None

template_struct = StructType([
    StructField("heading", StringType(), False),
    StructField("language", StringType(), False),
    StructField("misspelling_of", StringType(), False),
])

missp_schema = ArrayType(
    StructType([
        StructField("l2_heading", StringType(), False),
        StructField("num_definitions", IntegerType(), False),
        StructField("num_templates", IntegerType(), False),
        StructField("templates", ArrayType(template_struct), False),
    ])
)

def misspelling_of_parser(all_templates, text):
    '''
    Take a wikitext and parse it into a list of {heading, language, misspelling_of_word} dictionary.
    Each text may have one or more such dicts for various POS for example.
    Assumptions: 
        - L2 headings are languages
        - The # character starts lines that are definitions of the word. So if there is only one # line
          and it's a misspelling template, that is a confident way of stating that this word is only ever
          a misspelling.
        
    '''    
    wikicode = mwparserfromhell.parse(text, skip_style_tags=True)
        
    # list of list containing templates in all l2 sections
    all_tls = []

    # get templates in each L2 heading containing section
    # L2 heading containing sections are considered independent
    for l2_section in wikicode.get_sections(levels=[2]):
        
        # get the language heading
        l2_section_heading = l2_section.get(0).title.strip().lower()
        
        num_of_li = 0
        num_of_tls = 0
        tls = []
        
        for section in l2_section.get_sections(flat=True, include_lead=False):
            
            templates = []
            
            # get section header (e.g Noun, Adjective)
            section_heading = section.get(0).title.strip().lower()

            to_exclude = ("#;", "#:", "#*", "##")
            # Traverse the lines in this section
            for line in str(section).split('\n'):
                # look for definitions (numbered list starting with #)
                # avoid edge case where line starts with #*
                if line.startswith("#") and not line.startswith(to_exclude):
                    num_of_li += 1
                    # look for templates
                    parsed_line = mwparserfromhell.parse(line, skip_style_tags=True)
                    templates += parsed_line.filter_templates(matches = lambda x: str(x.name).strip().replace(" ","_").lower() in all_templates)

            num_of_tls += len(templates)
            
            # a header might have multiple templates in it, loop through all of them
            # language and misspelling_of are required params for these templates
            for template in templates:
                language = None
                misspelling_of = None
                
                try:
                    language = template.get("lang").value
                except Exception:
                    pass
                
                if language:
                    misspelling_of = template.get(1).value
                else:
                    language = template.get(1).value
                    misspelling_of = template.get(2).value

                tls.append({
                    "heading": str(section_heading),
                    "language": str(language),
                    "misspelling_of": str(misspelling_of)
                })
                
        # save all templates in this l2 section
        all_tls.append({
            "l2_heading": l2_section_heading,
            "num_definitions": num_of_li,
            "num_templates": num_of_tls,
            "templates": tls
        })
        
    return all_tls

def get_redirects(template_name, snapshot, dbname, namespace):
    # gets page ids of all templates that redirect to {template_name}
    # namespace 10 is template
    page_ids = spark.sql("select * from wmf_raw.mediawiki_redirect")\
                    .filter(f"wiki_db = '{dbname}'")\
                    .filter(f"snapshot = '{snapshot}'")\
                    .filter(f"rd_namespace = {namespace}")\
                    .filter(f"rd_title = '{template_name}'")\
                    .select("rd_from")\
                    .rdd.flatMap(lambda x: x).collect()
    
    # list to str
    page_ids = ', '.join([str(x) for x in page_ids])
    
    redirects = []
    
    # get titles of the pages from page_id list in {page_ids}
    if len(page_ids) > 0:
        redirects = spark.sql("select * from wmf_raw.mediawiki_page")\
                            .filter(f"wiki_db = '{dbname}'")\
                            .filter(f"snapshot = '{snapshot}'")\
                            .filter(f"page_namespace = {namespace}")\
                            .filter(f"page_id in ({page_ids})")\
                            .select("page_title")\
                            .rdd.flatMap(lambda x: x).collect()
    return redirects

def get_lt_ids(template_names, snapshot, dbname, namespace):
    # lt_id is NOT page_id
    # namespace 10 is template
    joiner = "\', \'"
    template_names_str = f"'{joiner.join(template_names)}'"
    return spark.sql("select * from wmf_raw.mediawiki_linktarget")\
                .filter(f"wiki_db = '{dbname}'")\
                .filter(f"snapshot = '{snapshot}'")\
                .filter(f"lt_namespace = {namespace}")\
                .filter(f"lt_title IN ({template_names_str})")\
                .select("lt_id")\
                .rdd.flatMap(lambda x: x).collect()

def get_pages(lt_ids, snapshot, dbname, namespace):
    # gets all pages that contain the given template(s)
    # namespace 0 is main page
    
    # list to str
    lt_ids = ', '.join([str(x) for x in lt_ids])
    
    return spark.sql("select * from wmf_raw.mediawiki_templatelinks")\
                .filter(f"wiki_db = '{dbname}'")\
                .filter(f"snapshot = '{snapshot}'")\
                .filter(f"tl_from_namespace = {namespace}")\
                .filter(f"tl_target_id IN ({lt_ids})")\
                .select("tl_from")\
                .distinct()
                                                                       
def get_wikitext(page_ids, snapshot, dbname, namespace):
    # have to use the same namespace as `get_pages`
    
    page_ids = page_ids.withColumnRenamed("tl_from", "page_id")

    return spark.sql("select * from wmf.mediawiki_wikitext_current")\
            .filter(f"wiki_db = '{dbname}'")\
            .filter(f"snapshot = '{snapshot}'")\
            .filter(f"page_namespace = {namespace}")\
            .join(page_ids, how="inner", on="page_id")\
            .select("page_id", "page_title", "revision_text")

def get_misspellings(snapshot, dbname, template_codes):
    extracted_templates_map = {}
    
    for template_name in template_codes:
        redirects = get_redirects(template_name, snapshot, dbname, 10)
        all_templates = [template_name]+redirects
        lt_ids = get_lt_ids(all_templates, snapshot, dbname, 10)
        pages_ids = get_pages(lt_ids, snapshot, dbname, 0)
        pages = get_wikitext(pages_ids, snapshot, dbname, 0)

        func = template_codes[template_name]["function"]
        schema = template_codes[template_name]["schema"]
        # normalize template names
        formatted_templates = [x.replace(" ","_").lower() for x in all_templates]

        parser_func_udf = F.udf(lambda x: func(formatted_templates,x), schema)
        extracted_templates = pages.withColumn("parsed_templates", parser_func_udf("revision_text"))\
                                    .withColumn("dbname", F.lit(dbname))\
                                    .withColumn("template", F.lit(template_name))\
                                    .withColumn("parsed_template", F.explode("parsed_templates"))\
                                    .select("dbname", "template", "page_id", "page_title", "parsed_template.*")\
                                    .filter("num_templates > 0")

        extracted_templates_map[template_name] = extracted_templates
    
    return extracted_templates_map

# to create template_codes map
# example final template_codes
#     {
#         "misspelling_of": {"function": misspelling_of_parser, "schema": missp_schema},
#         "deliberate_misspelling_of": {"function": misspelling_of_parser, "schema": missp_schema},
#     }
def populate_template_code(template_codes, template_name, function = misspelling_of_parser, schema = missp_schema):
    template_codes[template_name] = {"function": function, "schema": schema}
    return template_codes

def extract_and_save_misspelling(snapshot, dbname, filename, _spark, template_codes = {}):
    global spark
    
    spark = _spark
        
    # if template_codes was not populated, use english name of template as default
    if not template_codes:
        template_codes = populate_template_code(template_codes, "misspelling_of")
    
    misspellings_df_map = get_misspellings(snapshot=snapshot, dbname=dbname, template_codes=template_codes)
    misspellings = reduce(DataFrame.union, list(misspellings_df_map.values()))

    df = misspellings.toPandas()

    def spark_row_to_tuples(row):
        return [(v[0], v[1], v[2]) for v in row["templates"]]

    df["templates"] = df.apply(spark_row_to_tuples, axis=1)

    df.to_csv(filename, sep="\t", index=False)