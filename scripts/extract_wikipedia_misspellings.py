import re
import fasttext
import pandas as pd
import mwparserfromhell
from ftlangdetect import detect
from pyspark.sql.types import *
import pyspark.sql.functions as F
import wikitext_parser

spark = None

struct = StructType([
        StructField("section_heading", StringType(), False),
        StructField("misspelling", StringType(), False),
        StructField("suggested_word", ArrayType(StringType()), False),
        StructField("formatting", ArrayType(StringType()) , False), # formatting, is_all_caps, is_first_cap, is_list, is_quotes, is_different_language
        StructField("word_length", StringType(), False),
        StructField("detected_language_confidence", FloatType(), True),
        StructField("text_sent_to_model", StringType(), True),
        StructField("detected_language", StringType(), True),
        StructField("surrounding_text", StringType(), False),
    ])

def load_misspellings(filename):
    
    def de_struct(row):
        row["pos"]=row["templates"][0]
        row["ln"]=row["templates"][1]
        row["word"]=row["templates"][2]
        return row

    # load misspellings
    misspellings = pd.read_csv(filename, sep="\t").dropna()
    misspellings["templates"] = misspellings["templates"].apply(lambda x: eval(x))

    # explode each misspelling-correct word pair
    misspellings = misspellings.explode("templates").apply(de_struct, axis=1)

    # filter misspellings
    misspellings = misspellings[misspellings["num_definitions"]==misspellings["num_templates"]]
    
    return misspellings

def load_exploded_misspellings(filename):
    misspellings = pd.read_csv(filename, sep="\t").dropna()
    return misspellings


def get_target_list(lang_id, misspellings):
    target_list = dict()
    lang_df = misspellings[misspellings["ln"]==lang_id]
    lang_df = lang_df[["page_title", "word"]].drop_duplicates()

    for misspelling, miss_df in lang_df.groupby("page_title"):
        target_list[misspelling] = miss_df["word"].values.tolist()
        
    return target_list


def get_quotations_symbols(filename="quotation_symbols.txt"):

    quotations = []

    with open(filename) as file:
        for line in file:
            line = line.strip()
            if len(line)==2:
                quotations.append((line[0], line[1]))

    quotations_set = list(set(quotations))

    return quotations_set


def get_len(text):
    # split and join again to remove all extra space and \n 
    text = ' '.join(text.split())
    text_len = len(text)
    if text_len > 0:
        text_len += 1 # +1 to account for the space that will be added after it when concatenating
    
    return text_len


def get_different_language_from_table_cell(text, formatting, lang_id, lang_threshold, len_threshold, found):
    # set language information in each node's data
    is_different_language = False
    detected_language_confidence = None
    text_sent_to_model = None
    detected_language = None

    text = text.replace('\n', ' ').strip()
    # is the data in a table cell of different language? NB: each cell is a different node
    if formatting=="is_table" and len(text)>len_threshold:
        detection = detect(text, True)
        lang, score = detection["lang"], detection["score"]
        if lang!=lang_id and score>lang_threshold:
            is_different_language = True
            detected_language_confidence = score
            text_sent_to_model = text
            detected_language = lang

    for str_ix, data in found:
        if is_different_language:
            data["formatting"].append("is_different_language")
        data["detected_language_confidence"] = detected_language_confidence
        data["text_sent_to_model"] = text_sent_to_model
        data["detected_language"] = detected_language
        
    return found


def get_data(str_ix, text, target_words_list, heading, formatting, is_list, context_text, lang_id, lang_threshold, len_threshold):
    """
    Takes a node text `text` and node start index `str_ix` as input and returns a list of misspellings detected in this node.
    Returned list contains a pair: (start index of the misspelled word: `str_ix`, the misspelling data: `data`)
    Returns empty list if no misspellings were found.
    """
    found = []
    
    # to handle tables without a table tag
    if text.strip().startswith('|'):
        formatting = "table"
            
    formatting = "is_"+formatting.lower().replace(" ", "_")
    
    for word in text.split():
        if word in target_words_list:
            formatting_list = [formatting]
            
            if is_list:
                formatting_list.append("is_list")
            
            capitalization = "is_all_caps" if word.isupper() else "is_first_cap" if word[0].isupper() else False
            if capitalization:
                formatting_list.append(capitalization)
            
            data = {
                "section_heading": heading,
                "misspelling": word,
                "formatting": formatting_list,
                "word_length": len(word),
                "suggested_word": target_words_list[word],
                "surrounding_text": ' '.join([t for f,t in context_text]),
            }
            found.append((str_ix, data))
        
        str_ix += len(word)+1 # +1 to account for the space that will be added after it when concatenating
    
    if found:
        found = get_different_language_from_table_cell(text, formatting, lang_id, lang_threshold, len_threshold, found)
    
    return found


def get_quote_spans(text, quotations):
    """
    Finds quotes in `text`, and returns a list of the start and end indices of quotes as a pair.
    """
    spans = []
    for left_q, right_q in quotations:
        pattern = r'(^|\W){}(.+?){}(\W|$)'.format(left_q, right_q)
        for m in re.finditer(pattern, text):
            # get start and end indices of 2nd group, which is the required string
            i1 = m.start(2)
            i2 = m.end(2)
            spans.append((i1, i2))
    return spans


def get_quotes(found_misspellings_in_section, section_text, quotations):
    """
    Find and mark misspellings within quotes.
    """
    # look for quotes if the section has misspellings
    if found_misspellings_in_section:
        quote_spans = get_quote_spans(section_text, quotations)
        for i1, i2 in quote_spans:
            for i, data in found_misspellings_in_section:
                # Check that the start index of the misspelled word is within a quotes range.
                # We assume a quotation always ends after a word, and so do not check if the end indices of words fall in quotes range.
                if i1<=i and i<i2:
                    data["formatting"].append("is_quote")
                elif i>i2:
                    # rest of the misspellings will also be out of range
                    break
    
    return found_misspellings_in_section


def get_words(wikitext, target_words_list, lang_id, lang_threshold, len_threshold, quotations):
    """
    Gets the occurences of each word in `target_words_list` in the wikicode,
    along with surrounding text and section title
    """
    found_misspellings = []
    
    sections = mwparserfromhell.parse(wikitext).get_sections(flat=True)
    for section in sections:
        found_misspellings_in_section = []
        heading = ""
        try:
            heading = section.get(0).title.strip().lower()
        except:
            pass
        
        is_list = False
        
        parsed_text = wikitext_parser.wikitext_to_plaintext(section)
        str_ix = 0
        
        for i, (formatting, text) in enumerate(parsed_text):
            context_text = parsed_text[max(0, i-5):i+5]
            
            if formatting == "List":
                # list starts
                is_list = True # `List` does not have text
            elif is_list and ('\n' in text):
                # list continues, reached the list item. The last item is of the form "# some text\nmore text".
                # Here "# some text" is the list item, and "more text" is plain text that occurs after the list.
                texts = text.split('\n', maxsplit=1)
                
                found_misspellings_in_section += get_data(str_ix, texts[0], target_words_list, heading, formatting,
                                                          is_list, context_text, lang_id, lang_threshold, len_threshold)
                str_ix += get_len(texts[0])
                
                is_list = False
                
                found_misspellings_in_section += get_data(str_ix, texts[1], target_words_list, heading, formatting,
                                                          is_list, context_text, lang_id, lang_threshold, len_threshold)
                str_ix += get_len(texts[1])
            else:
                # normal case
                found_misspellings_in_section += get_data(str_ix, text, target_words_list, heading, formatting,
                                                          is_list, context_text, lang_id, lang_threshold, len_threshold)
                str_ix += get_len(text)
        
        
        section_text_raw = ' '.join([text for _, text in parsed_text])
        section_text = ' '.join(section_text_raw.split()) # split and join again to remove all extra space and \n
        
        # set is_quote
        found_misspellings_in_section = get_quotes(found_misspellings_in_section, section_text, quotations)
                        
        # detect language in paragraphs if the section has misspellings
        if found_misspellings_in_section:
            paragraphs = section_text_raw.split("\n\n")
            # split and join again to remove all extra space and \n for each paragraph
            paragraphs = [" ".join(para.split()) for para in paragraphs]
            start_ix = 0
            
            for para in paragraphs:
                if len(para)==0:
                    continue # avoid +1 in end_ix
                
                end_ix = start_ix + len(para)
                detection = detect(para, True)
                lang, score = detection["lang"], detection["score"]
                if lang!=lang_id and score>lang_threshold:
                    # all misspellings in this para will have `is_different_language`
                    for i, data in found_misspellings_in_section:
                        if start_ix<=i and i<end_ix:
                            if "is_different_language" not in data["formatting"]:
                                data["formatting"].append("is_different_language")
                                data["detected_language_confidence"] = score
                                data["text_sent_to_model"] = para
                                data["detected_language"] = lang
                        elif i>end_ix:
                            # rest of the misspellings will also be out of range
                            break
                
                start_ix = end_ix+1 # +1 for space added when concatenating
        
            # print(str_ix, len(section_text), end_ix) # 4337 4336 4336
        
        # discard the start_index, create a list of misspelling data
        found_misspellings += [data for _, data in found_misspellings_in_section]
    
    return found_misspellings


def extract_wikitext_misspellings(target_words_list, dbname, snapshot, namespace, lang_id, lang_threshold, len_threshold, quotations):
    """
    Parses wikitext of all main pages in `dbname` language wikipedia (e.g enwiki) and
    finds frequency of target words in the entire wikipedia.
    """

    # get the latest revision of wikitexts (each snapshot should have only 1 version)
    # remove all redirect and empty pages
    df = spark.sql("select * from wmf.mediawiki_wikitext_current")\
                .filter(f"wiki_db = '{dbname}'")\
                .filter(f"snapshot = '{snapshot}'")\
                .filter(f"page_namespace = {namespace}")\
                .filter(F.col("revision_text").isNotNull())\
                .filter(F.length(F.col("revision_text")) > 0)\
                .filter(f"page_redirect_title = ''")\
                .select("page_id", "page_title", "revision_text")

    # parse and filter the wikicode
    frequency_udf = F.udf(lambda x: get_words(x, target_words_list, lang_id, lang_threshold, len_threshold, quotations), ArrayType(struct))
    
    df = df.select("page_id", "page_title", frequency_udf(F.col("revision_text")).alias("misspelling_text"))\
            .filter("size(misspelling_text) > 0")\
            .withColumn("misspelling_text", F.explode("misspelling_text"))\
            .select("page_id", "page_title", "misspelling_text.*")
    
    return df


def extract_and_save_misspelling(dbname, snapshot, language_confidence_threshold, table_cell_character_length_threshold, 
                                 misspellings_file, misspellings_file_exploded, quotations_file, filename, _spark):
    global spark
    spark = _spark
    
    language = "en" if dbname == "simplewiki" else dbname[:-4]
    filename = filename.format(dbname) # filename would have a placeholder for wiki, like: "../outputs/{}_detected_misspelling.tsv"
    
    quotations_set = get_quotations_symbols(quotations_file)
    
    if misspellings_file_exploded:
        misspellings = load_exploded_misspellings(misspellings_file)
    else:
        misspellings = load_misspellings(misspellings_file)
        
    target_list = get_target_list(language, misspellings)
    
    if target_list:
        # if some misspellings are present in that language
        misspelling_freq_df = extract_wikitext_misspellings(target_list, 
                                                            dbname=dbname, 
                                                            snapshot=snapshot, 
                                                            namespace=0, 
                                                            lang_id=language, 
                                                            lang_threshold=language_confidence_threshold, 
                                                            len_threshold=table_cell_character_length_threshold,
                                                            quotations=quotations_set).toPandas()

        misspelling_freq_df.to_csv(filename, sep="\t", index=False)
